# Deep learning

## Guide du repo

* `Tutoriel_deeplearning.ipynb` montre les bases du deeplearning: algorithme de classification avec scikit-learn ou bien keras

## Setup python environment

Create and activate the python virtual environment
```sh
python3 -m venv deeplearning
source deeplearning/bin/activate
```

Install the desired pip packages

```sh
pip install -r requirements.txt
``` 

Install the kernel in jupyter notebook

```sh
python -m ipykernel install --user --name=deeplearning --display-name "Deep learning"
```

Finally start the jupyter notebook

```sh
jupyter notebook
```

### Summary of the setup

```sh
python3 -m venv deeplearning
source deeplearning/bin/activate
pip install numpy matplotlib scikit-learn tensorflow
pip install ipykernel jupyter
pip freeze > requirements.txt
python -m ipykernel install --user --name=deeplearning --display-name "Cours deep learning"
jupyter notebook
```
